import {MATRICULA_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'

const getMatriculaUrl=(id)=>`${MATRICULA_ENDPOINT}/${id}`
class MatriculaService{
    static fetchMatriculas=()=>baseApi.get(MATRICULA_ENDPOINT)
    static fetchMatricula=(id)=>baseApi.get(getMatriculaUrl(id))
    static fetchDetalleMatricula=(id)=>baseApi.get(`${MATRICULA_ENDPOINT}/detalle/${id}`)
    static addMatricula=(matricula)=>baseApi.post(MATRICULA_ENDPOINT,matricula)
    static updateMatricula=(matricula)=>baseApi.put(getMatriculaUrl(matricula.id),matricula)
    static removeMatricula=(id)=>baseApi.delete(getMatriculaUrl(id))
}
export default MatriculaService