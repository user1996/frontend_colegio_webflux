import {CURSO_ENDPOINT} from '../core/appConstants'
import baseApi from './baseApi'

const getCursoUrl=(id)=>`${CURSO_ENDPOINT}/${id}`

class CursoService{
    static fetchCursos=()=>baseApi.get(CURSO_ENDPOINT)
    static fetchCurso=(id)=>baseApi.get(getCursoUrl(id))
    static addCurso=(curso)=>baseApi.post(CURSO_ENDPOINT,curso)
    static updateCurso=(curso)=>baseApi.put(getCursoUrl(curso.id),curso)
    static removeCurso=(id)=>baseApi.delete(getCursoUrl(id))
    static changeStateCurso=(id,estado)=>baseApi.get(`${CURSO_ENDPOINT}/cambiaEstado/${id}/${estado}`)
}
export default CursoService