import {ROL_ENDOINT} from '../core/appConstants'
import baseApi from './baseApi'

const getRolUrl=(id)=>`${ROL_ENDOINT}/${id}`

class RolService{
    static fetchRols=()=>baseApi.get(ROL_ENDOINT)
    static fetchRol=(id)=>baseApi.get(getRolUrl(id))
    static addRol=(rol)=>baseApi.post(ROL_ENDOINT,rol)
    static updateRol=(rol)=>baseApi.put(getRolUrl(rol.id),rol)
    static removeRol=(id)=>baseApi.delete(getRolUrl(id))
}
export default RolService
