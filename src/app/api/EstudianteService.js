import {ESTUDIANTE_ENPOINT} from '../core/appConstants'
import baseApi from './baseApi'

const getEstudianteUrl=(id)=>`${ESTUDIANTE_ENPOINT}/${id}`
class EstudianteService{
    static fetchEstudiantes=()=>baseApi.get(ESTUDIANTE_ENPOINT)
    static fetchEstudiante=(id)=>baseApi.get(getEstudianteUrl(id))
    static addEstudiante=(estudiante)=>baseApi.post(ESTUDIANTE_ENPOINT,estudiante)
    static updateEstudiante=(estudiante)=>baseApi.put(getEstudianteUrl(estudiante.id),estudiante)
    static removeEstudiante=(id)=>baseApi.delete(getEstudianteUrl(id))
}
export default EstudianteService