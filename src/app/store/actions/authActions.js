import { toast } from 'react-toastify'
import * as actionTypes from './actionTypes'
import AuthService from '../../api/authService'
import RolService from '../../api/rolService'
import { closeModal } from './modalActions'
import history from '../../..'

const loginUser = () => {
  return { type: actionTypes.LOGIN_USER }
}

const setCurrentUser = (currentUser) => {
  return { type: actionTypes.CURRENT_USER, payload: { currentUser } }
}

const signOutUser = () => {
  return { type: actionTypes.LOGOUT_USER }
}

export const getUser = () =>async (dispatch) => {
  try {
    const user =await AuthService.currentUser()
    user.allRoles=await RolService.fetchRols()
    dispatch(setCurrentUser(user))
  } catch (error) {
    toast.error(error)
  }
}

export const login = (credentials) => async (dispatch) => {
  try {
    await AuthService.login(credentials)
    dispatch(loginUser())
    dispatch(getUser())
    dispatch(closeModal())
    toast.success('Bienvenido al Sistema de Matriculas')
    history.push('/matriculas')
  } catch (error) {
    throw error
  }
}

export const logout = () => (dispatch) => {
  try {
    AuthService.logout()
    dispatch(signOutUser())
    history.push('/')
  } catch (error) {
    toast.error(error)
  }
}
