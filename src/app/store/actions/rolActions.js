import { toast } from 'react-toastify'
import * as actionTypes from './actionTypes'
import RolService from '../../api/rolService'

const loadingRoles=(loading)=>{
  return {type:actionTypes.LOADING_ROLES,payload:{loading}}
}

const fetchRolesAction=(roles)=>{
  return {type:actionTypes.FETCH_ROLES,payload:{roles}}
}

export const fetchRoles=()=>async(dispatch)=>{
  dispatch(loadingRoles(true))
  try {
    const roles=await RolService.fetchRols()
    dispatch(fetchRolesAction(roles))
    dispatch(loadingRoles(false))
  } catch (error) {
    dispatch(loadingRoles(false))
    toast.error('Error al cargar roles')
  }
}