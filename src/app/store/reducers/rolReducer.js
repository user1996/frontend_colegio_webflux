import createReducer from './reducerUtils'
import { LOADING_ROLES,FETCH_ROLES} from '../actions/actionTypes'

const initialState = {
    roles:[],
    loadingRoles:false
  }

  const loadingRoles=(state,payload)=>{
    return {...state,loadingRoles:payload.loading}
  }
  const fetchRoles=(state,payload)=>{
    return {...state,roles:payload.roles}
  }
  export default createReducer(initialState,{
    [LOADING_ROLES]:loadingRoles,
    [FETCH_ROLES]:fetchRoles
  })