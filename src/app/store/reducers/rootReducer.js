import {combineReducers} from 'redux'
import modalReducer from './modalReducer'
import authReducer from './authReducer'
import rolReducer from './rolReducer'
const rootReducer=combineReducers({
    modal:modalReducer,
    auth:authReducer,
    rol:rolReducer
})
export default rootReducer