import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { ToastContainer } from 'react-toastify'
import PropTypes from 'prop-types'
import LoadingComponent from '../../component/common/LoadingComponent'
import ModalContainer from '../../component/modal/ModalContainer'
import { getJwt } from '../config/auth/credentials'
import Routes from '../config/Routes'
import { getUser } from '../store/actions/authActions'
const actions = {
  getUser,
}

const mapState = (state) => ({
  authenticated: state.auth.authenticated,
  roles:state.auth.roles,
  userRol:state.auth.userRol
})

const App=({getUser,authenticated,roles,userRol}) => {
  const [appLoaded,setAppLoaded]=useState(false)
 useEffect(()=>{
   const token=getJwt()
   if(token){
     getUser()
   }
   setAppLoaded(true)
 },[getUser])
  if(!appLoaded)return <LoadingComponent content="Cargando app...."/>
  return (
    <>
      <ModalContainer />
      <ToastContainer position="bottom-right" />
      <Routes authenticated={authenticated} roles={roles} userRol={userRol}/>
  </>
  )
}
App.propTypes = {
  getUser: PropTypes.func.isRequired,
  authenticated: PropTypes.bool,
  roles:PropTypes.array,
  userRol:PropTypes.array
}

App.defaultProps = {
  
  authenticated: false,
}
export default connect(mapState,actions) (App)
