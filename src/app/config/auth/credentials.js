import jwDecode from 'jwt-decode'
import {TOKEN_KEY} from '../../core/appConstants'

export const setToken =(token) =>{
    if (token) localStorage.setItem(TOKEN_KEY,token)
}
export const getJwt=()=>localStorage.getItem(TOKEN_KEY)

export const getDecodeToken =() =>{
    try {
        return jwDecode(getJwt())
    } catch (error) {
        return null
    }
}
export const removeToken=()=>{
    localStorage.removeItem(TOKEN_KEY)
}