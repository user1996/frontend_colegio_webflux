import React from 'react'
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';
import { Container } from 'semantic-ui-react';
import PropTypes from 'prop-types'
import Navbarr from '../../component/navbar/Navbarr'
import HomePage from '../../pages/home/HomePage'
import Footer from '../../component/footer/Footer'
import privateRoutesConfig from '../../component/models/privateRoutesConfig'

const Routes = ({authenticated,userRol}) => {
  let routes
  if(userRol){
    const rol=userRol[0]
    const nuevo=[]
    privateRoutesConfig.forEach(item=>{
      if(item.permission && item.permission.filter(i=>i===rol).length>0){
        nuevo.push(item)
      }
    })
    if(privateRoutesConfig && privateRoutesConfig.length>0){
     routes=(<>{nuevo.map(item=>(<Route key={item.id} path={item.path} component={item.component}/>))}</>)
    }
  }  
  return (
        <>
        <Route exact path="/" component={HomePage}/>
        {authenticated ?(
          <Route
          path="/(.+)"
          render={() => (
            <>
              <Navbarr/>
              <Container style={{marginTop:'7em'}}>
                <Switch>
                  {routes}
                </Switch>
                </Container>
              <Footer/>
            </>
          )}
        />
        ):(
          <Redirect path='/' to='/'/>
        )}
        </>
    )
}
Routes.propTypes = {
  authenticated: PropTypes.bool,
  userRol:PropTypes.array
}

Routes.defaultProps = {
  authenticated: false,
}
export default withRouter (Routes)