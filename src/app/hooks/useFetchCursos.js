import { useState, useEffect } from 'react'
import { toast } from 'react-toastify'
import CursoService from '../api/CursoService'

const useFetchCursos = () => {
  const [cursos, setCursos] = useState(null)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    CursoService.fetchCursos()
      .then((response) => {
        setCursos(response)
      })
      .catch((error) => {
        toast.error(error)
      })
    setLoading(false)
  }, [])

  return [cursos, loading]
}

export default useFetchCursos