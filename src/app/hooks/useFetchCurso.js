import { useEffect, useState } from 'react'
import CursoService from '../api/CursoService'
const useFetchCurso = (id) => {
    const [curso,setCurso]=useState(null)
    const [loading,setLoading]=useState(false)
    useEffect(()=>{
        setLoading(true)
        if(id)
            CursoService.fetchCurso(id)
            .then((response)=>{
                setCurso(response)
            })
            .catch((error)=>{
                alert('Error '+error)
            })
            setLoading(false)
    },[id])
    return [curso,loading]
}

export default useFetchCurso
