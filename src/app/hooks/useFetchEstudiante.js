import { useEffect, useState } from 'react'
import EstudianteService from '../api/EstudianteService'
const useFetchEstudiante = (id) => {
    const [estudiante,setEstudiante]=useState(null)
    const [loading,setLoading]=useState(false)
    useEffect(()=>{
        setLoading(true)
        if(id)
            EstudianteService.fetchEstudiante(id)
            .then((response)=>{
                setEstudiante(response)
            })
            .catch((error)=>{
                alert('Error '+error)
            })
            setLoading(false)
    },[id])
    return [estudiante,loading]
}

export default useFetchEstudiante
