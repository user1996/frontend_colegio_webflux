import { useEffect, useState } from 'react'
import MatriculaService from '../api/MatriculaService'
const useFetchMatricula = (id) => {
    const [matricula,setMatricula]=useState(null)
    const [loading,setLoading]=useState(false)
    useEffect(()=>{
        setLoading(true)
        if(id)
            MatriculaService.fetchMatricula(id)
            .then((response)=>{
                setMatricula(response)
            })
            .catch((error)=>{
                alert('Error '+error)
            })
            setLoading(false)
    },[id])
    return [matricula,loading]
}

export default useFetchMatricula