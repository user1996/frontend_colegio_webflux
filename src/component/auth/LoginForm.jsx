import React from 'react'
import {Field, Form as FinalForm} from 'react-final-form'
import {combineValidators, isRequired} from 'revalidate'
import {FORM_ERROR} from 'final-form'
import {Form,Header,Button} from 'semantic-ui-react'
import PropTypes from 'prop-types'

import TextInput from '../form/TextInput'
import ErrorMessage from '../form/ErrorMessage'
import {login} from '../../app/store/actions/authActions'
import { connect } from 'react-redux'
const validate=combineValidators({
    username:isRequired('username'),
    password:isRequired('password')
})
const actions={
    login
}
const LoginForm = ({login}) => {
    return (
        <FinalForm
            onSubmit={(values)=>login(values).catch((error)=>({[FORM_ERROR]:error}))}
            validate={validate}
            render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
                <Form onSubmit={handleSubmit} error>
                    <Header as="h2" content='Login' color='grey' textAlign='center'/>
                    <Field name="username" component={TextInput} placeholder="Escriba el nombre de Usuario"/>
                    <Field name="password" component={TextInput} placeholder="Escriba la clave de usuario" type='password'/>
                    {submitError && !dirtySinceLastSubmit && (
                        <ErrorMessage error={submitError} text='Usuario o clave invalidos'/>
                    )}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                        loading={submitting}
                        color='violet'
                        content='login'
                    />
                </Form>
            )}
        />
    )
}
LoginForm.propTypes = {
    login: PropTypes.func.isRequired,
  }
export default connect(null,actions) (LoginForm)
