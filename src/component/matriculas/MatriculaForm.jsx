import React, { useState,useEffect } from 'react'
import { Field, Form as FinalForm } from 'react-final-form'
import { combineValidators, composeValidators, isRequired } from 'revalidate'
import { Button, Container, Divider, Form, Grid, Header, Popup, Table } from 'semantic-ui-react'
import history from '../..'
import {toast} from 'react-toastify'
import ErrorMessage from '../form/ErrorMessage'
import SelectInput from '../form/SelectInput'
import useFetchEstudiantes from '../../app/hooks/useFetchEstudiantes'
import useFecthCursos from '../../app/hooks/useFetchCursos'
import MatriculaService from '../../app/api/MatriculaService'
const validate = combineValidators({
    estudiante: composeValidators(
      isRequired({message:'Nombre de curso es obligatorio'})
    )(),
    curso: composeValidators(
        isRequired({message:'Siglas son obligatorias'})
    )()
  })
const MatriculaForm = () => {
    const [estudiantes] = useFetchEstudiantes()
    const [estudiantesList, setEstudiantesList] = useState([])
    const [cursos] = useFecthCursos()
    const [cursosList, setCursosList] = useState([])
    const [loadingEstudiantes, setLoadingEstudiantes] = useState(true)
    const [loadingCursos, setLoadingCursos] = useState(true)
    const [items, setItems] = useState([])
    const [item, setItem] = useState(null)

    useEffect(()=>{
        setLoadingEstudiantes(true)
        if(cursos){
            const cursosList=[]
            cursos.forEach((item)=>{
                const curso={
                    key:item.id,
                    text: `${item.nombreCurso}  -  ${item.siglas}`,
                    value:item.id,
                    disabled:!item.estado,
                    label:{color:item.estado?'green':'red',empty:true,circular:true}
                }
                cursosList.push(curso)
            })
            setCursosList(cursosList)
            setLoadingCursos(false)
        }

        setLoadingEstudiantes(true)
        if(estudiantes){
            const estudiantesList=[]
            estudiantes.forEach((item)=>{
                const estudiante={
                    key:item.id,
                    text: `${item.nombres} ${item.apellidos}`,
                    value:item.id
                }
                estudiantesList.push(estudiante)
            })
            setEstudiantesList(estudiantesList)
            setLoadingEstudiantes(false)
        }
    },[estudiantes,cursos])

    const handleAddingItems=()=>{
        const newItems=[...items]
        const cursosList=[...cursos]
        const index=newItems.findIndex((a)=>a.id===item)
        if(index>-1){
            newItems[index]={
                id:newItems[index].id,
                name:newItems[index].name,
                precio:newItems[index].precio
            }
            setItems(newItems)
        }else{
            const newItem={
                id:item,
                name:cursosList.filter((a)=>a.id===item)[0].nombreCurso,
                precio:cursosList.filter((a)=>a.id===item)[0].precio
            }
            newItems.push(newItem)
        }
        newItems.total=totalCurso(newItems)
        setItems(newItems)
    }
    const handleRemoveItems=(id)=>{
        let updateItems=[...items]
        updateItems=updateItems.filter((a)=>a.id!==id)
        updateItems.total=totalCurso(updateItems)
        setItems(updateItems)
    }
    const totalCurso=(itemss)=>itemss.reduce((sum,{precio})=>sum+precio,0)

    const handleAddNewMatricula=async(values)=>{
        const newItems=[...items]
        const itemsForMatricula=newItems.map((item)=>{
            return {id:item.id}
        })
        const newMatricula={
            estudiante:{id:values.estudiante},
            cursos:itemsForMatricula,
            estado:true
        }
        try {
            const matricula=await MatriculaService.addMatricula(newMatricula)
            toast.info('Alumno matriculado',
                    {
                        position:"top-center",
                        autoClose:2000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
                history.push(`matricula/${matricula.id}`)
        } catch (error) {
            toast.error('Error '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
    }
    return (
        <FinalForm
            onSubmit={(values) => handleAddNewMatricula(values)}
            validate={validate}
            render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
                <Form onSubmit={handleSubmit} error loading={loadingCursos || loadingEstudiantes}>
                  <Form.Group widths="equal">
                    <Field name="estudiante" component={SelectInput} placeholder="Seleccione estudiante" options={estudiantesList}/>
                    <Field name="curso" component={SelectInput} placeholder="Selecciona un Curso" options={cursosList} handleOnChange={(e) => setItem(e)}/>
                    <Popup inverted content="Agregar Curso a la matricula" trigger={<Button
                                        type="button"
                                        loading={submitting}
                                        color="violet"
                                        icon="plus circle"
                                        onClick={handleAddingItems}
                                        disabled={!item}
                                    />
                                }
                                />
                  </Form.Group>
                  <Container>
                      <Grid centered columns={2}>
                          <Grid.Column>
                          {items && items.length>0 ?(
                                <Table >
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell>Curso</Table.HeaderCell>
                                            <Table.HeaderCell>Precio</Table.HeaderCell>
                                            <Table.HeaderCell></Table.HeaderCell>
                                            <Table.HeaderCell/>
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {items.map((item)=>(
                                            <Table.Row key={item.id}>
                                                <Table.Cell>{item.name}</Table.Cell>
                                                <Table.Cell>{item.precio}</Table.Cell>
                                                <Table.Cell>
                                                    <Popup
                                                        inverted
                                                        content="Remover de la Lista"
                                                        trigger={
                                                            <Button
                                                                color="red"
                                                                icon="remove circle"
                                                                type="button"
                                                                onClick={()=>handleRemoveItems(item.id)}
                                                            />
                                                        }
                                                    />
                                                </Table.Cell>
                                            </Table.Row>
                                        ))}
                                    </Table.Body>
                                    <Table.Footer fullWidth>
                                        <Table.Row>
                                            <Table.Cell><strong>Total :</strong></Table.Cell>
                                            <Table.Cell content={items.total}/>
                                        </Table.Row>
                                    </Table.Footer>
                                </Table>
                            ):(
                                <Divider horizontal>
                                    <Header as='h4'>
                                    Sin Registros
                                    </Header>
                                </Divider>
                            )}
                          </Grid.Column>
                      </Grid><br/>
                      {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalid Values" />}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine || items.length===0}
                        loading={submitting}
                        color="violet"
                        content="Agregar nueva matricula"
                    />
                  </Container>
                </Form>
              )}
        />
    )
}

export default MatriculaForm
