import React from 'react'
import {Link} from 'react-router-dom'
import { Container, Dropdown, Image, Menu} from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { logout } from '../../app/store/actions/authActions'
import DropDownNavbar from '../../component/models/DropDownNavbar'
const mapState = (state) => ({
  currentUser: state.auth.currentUser,
})
const actions = {
  logout,
}
const Navbarr = ({currentUser,logout}) => {
  const rol=currentUser.roles[0]
  const menuArray=[]
  let dropDown;
  DropDownNavbar.forEach((item)=>{
  if(item.permission && item.permission.filter((item)=>item===rol).length>0){
    menuArray.push(item)
  }
}) 
    dropDown=(
      <>
      {menuArray.map((item)=>(
        <Dropdown key={item.id} item text={item.text}>
          <Dropdown.Menu>
            {item.menu.map((m)=>(
              <Dropdown.Item key={m.id} icon={m.icon} as={Link} to={m.to} text={m.text}/>
            ))}
          </Dropdown.Menu>
        </Dropdown>
      ))}
      </>
    )
    return (
      <>
      <Menu fixed='top' inverted>
      <Container>
        <Menu.Item as={Link} to='/' header>
        <Image size='mini' src='https://pngimage.net/wp-content/uploads/2018/06/icono-nuevo-registro-png-6.png' style={{ marginRight: '1.5em' }} />
          Registration System
        </Menu.Item>
        {dropDown}
        <Menu.Item as='a' position='right'>
          <Image avatar spaced="right" src="https://electronicssoftware.net/wp-content/uploads/user.png" />
          <Dropdown pointing="top left" text={currentUser.sub}>
            <Dropdown.Menu>
              <Dropdown.Item text={rol} icon="address card outline"/>
              <Dropdown.Item text="Logout" icon="log out" onClick={logout}/>
            </Dropdown.Menu>
          </Dropdown>
          </Menu.Item>
      </Container>
    </Menu>
      </>
    )
}
Navbarr.propTypes = {
  currentUser: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
}
export default connect(mapState,actions) (Navbarr)
