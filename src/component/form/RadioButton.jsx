import React from 'react'
import { Form, Radio } from 'semantic-ui-react'

const RadioButton = ({input,label}) => {
    return (
        <Form.Field>
            <Radio label={label} name={input.name} checked={input.checked} onChange={(e,{checked})=>input.onChange(input.value)}/>
        </Form.Field>
    )
}

export default RadioButton
