import roles from './roles'

import Curso from '../../pages/curso/Curso'
import Estudiantes from '../../pages/estudiante/Estudiantes'
import Matricula from '../../pages/matricula/Matricula'
import MatriculaDetalles from '../../pages/matricula/MatriculaDetalles'
import Matriculas from '../../pages/matricula/Matriculas'

export default [
    {
        id:1,
        component:Curso,
        path:'/cursos',
        permission:[
            roles.ADMIN,
            roles.DEV
        ]
    },
    {
        id:2,
        component:Estudiantes,
        path:'/estudiantes',
        permission:[
            roles.ADMIN,
            roles.DEV
        ]
    },
    {
        id:3,
        component:Matricula,
        path:'/matricula',
        permission:[
            roles.ADMIN,
            roles.DEV,
            roles.USER,
            roles.OPER
        ]
    },
    {
        id:4,
        component:MatriculaDetalles,
        path:'/matricula/:id',
        permission:[
            roles.ADMIN,
            roles.DEV,
            roles.USER,
            roles.OPER
        ]
    },
    {
        id:5,
        component:Matriculas,
        path:'/matriculas',
        permission:[
            roles.ADMIN,
            roles.DEV,
            roles.USER,
            roles.OPER
        ]
    }
]