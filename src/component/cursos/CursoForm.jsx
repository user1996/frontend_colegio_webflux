import React, { useEffect, useState } from 'react'
import {combineValidators,isRequired,composeValidators} from 'revalidate'
import {Field, Form as FinalForm} from 'react-final-form'
import {Button, Form,Header} from 'semantic-ui-react'
import PropTypes from 'prop-types'

import useFetchCurso from '../../app/hooks/useFetchCurso'
import TextInput from '../form/TextInput'
import  ErrorMessage  from '../form/ErrorMessage'
import CheckBox from '../form/CheckBox'


const validate = combineValidators({
    nombreCurso: composeValidators(
      isRequired({message:'Nombre de curso es obligatorio'})
    )(),
   
    siglas: composeValidators(
        isRequired({message:'Siglas son obligatorias'})
    )()
  })


const CursoForm = ({cursoId,handleSubmit}) => {
    
    const [actionLabel,setActionLabel]=useState('Agregar Curso')
    const [curso,loading]=useFetchCurso(cursoId)
    
    useEffect(()=>{
        if(cursoId){
            setActionLabel('Editar Curso')
        }else{
            setActionLabel('Agregar nuevo curso')
        }
    },[cursoId])

    return (
        <FinalForm
            onSubmit={(value)=>handleSubmit(value)}
            initialValues={cursoId && curso}
            validate={validate}
            render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
                <Form onSubmit={handleSubmit} error loading={loading}>
                    <Header as="h2" content={actionLabel} color="grey" textAlign="center"/>
                    <Field name="nombreCurso" component={TextInput} placeholder="Curso" />
                    <Field name="siglas" component={TextInput} placeholder="Siglas" />
                    <Field name="precio" component={TextInput} type='number' placeholder="Precio S/."/>
                    <Field name="estado" type='checkbox' label='Activo' component={CheckBox}/>
                    {submitError && !dirtySinceLastSubmit && (
                        <ErrorMessage error={submitError} text="Invalid username or password" />
                    )}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                        loading={submitting}
                        color='grey'
                        content={actionLabel}
                    />
                </Form>
            )}
        />
    )
}
CursoForm.propTypes={
    estudianteId:PropTypes.string,
    handleSubmit:PropTypes.func.isRequired
}
CursoForm.defaultProps={
    estudianteId:null
}
export default CursoForm
