import React, { useEffect, useState } from 'react'
import {combineValidators,isRequired,createValidator, composeValidators} from 'revalidate'
import {Field, Form as FinalForm} from 'react-final-form'
import {Button, Form,Header} from 'semantic-ui-react'
import PropTypes from 'prop-types'

import useFetchEstudiante from '../../app/hooks/useFetchEstudiante'
import TextInput from '../form/TextInput'
import RadioButton from '../form/RadioButton'
import  ErrorMessage  from '../form/ErrorMessage'
import { FORM_ERROR } from 'final-form'


  const isInvalidDate = createValidator(
    message => value => {
        if(new Date(value)>=new Date()){
            return message
        }
    },
    'Fecha de nacimiento no puede ser mayor a la de hoy'
  )
const validate = combineValidators({
    nombres: composeValidators(
      isRequired({message:'Ingresa nombres'})
    )(),
   
    apellidos: composeValidators(
        isRequired({message:'Ingresa apellidos'})
    )(),
   
    genero: composeValidators(
        isRequired({message:'Genero es requerido'})
    )(),
    dni: composeValidators(
        isRequired({message:'Dni es requerido'})
    )(),
    fechaNacimiento: composeValidators(
        isRequired({message:'Fecha es requerido'}),
        isInvalidDate
    )()
  })


const EstudianteForm = ({estudianteId,handleSubmit}) => {
    
    const [actionLabel,setActionLabel]=useState('Agregar Estudiante')
    const [estudiante,loading]=useFetchEstudiante(estudianteId)
    
    useEffect(()=>{
        if(estudianteId){
            setActionLabel('Editar Estudiante')
        }else{
            setActionLabel('Agregar nuevo estudiante')
        }
    },[estudianteId])

    return (
        <FinalForm
            onSubmit={(value)=>handleSubmit(value).catch((error)=>({[FORM_ERROR]:error}))}
            initialValues={estudianteId && estudiante}
            validate={validate}
            render={({handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit})=>(
                <Form onSubmit={handleSubmit} error loading={loading}>
                    <Header as="h2" content={actionLabel} color="grey" textAlign="center"/>
                    <Field name="dni" component={TextInput} placeholder="Ingresa Dni" />
                    <Field name="nombres" component={TextInput} placeholder="Ingresa nombres" />
                    <Field name="apellidos" component={TextInput} placeholder="Ingresa apellidos" />
                    <Field name="genero" component={RadioButton} label="M" value="M" type='radio'/><Field name="genero" component={RadioButton} label="F" value="F" type='radio'/>
                    <Field name="fechaNacimiento" component={TextInput} type="date" placeholder="Select una fecha" />
                    {submitError && !dirtySinceLastSubmit && (
                        <ErrorMessage error={submitError} text="Datos Incorrectos" />
                    )}
                    <Button
                        fluid
                        disabled={(invalid && !dirtySinceLastSubmit) || pristine}
                        loading={submitting}
                        color='grey'
                        content={actionLabel}
                    />
                </Form>
            )}
        />
    )
}
EstudianteForm.propTypes={
    estudianteId:PropTypes.string,
    handleSubmit:PropTypes.func.isRequired
}
EstudianteForm.defaultProps={
    estudianteId:null
}
export default EstudianteForm
