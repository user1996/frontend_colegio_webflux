import React from 'react';
import { Router } from 'react-router-dom'
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css'
import 'react-toastify/dist/ReactToastify.min.css'
import './app/layout/style.css'
import App from './app/layout/App';
import { createBrowserHistory } from 'history';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import configureStore from './app/store/configureStore'
const history = createBrowserHistory();
const store=configureStore()
const rootEl = document.getElementById('root');
const app=(
  <Provider store={store}>
    <Router history={history}>
    <App />
  </Router>
  </Provider>
)
ReactDOM.render(
 app,
 rootEl
);

//  If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
export default history;