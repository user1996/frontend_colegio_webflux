import React, { useCallback, useEffect, useState } from 'react'
import {connect} from 'react-redux'
import { Breadcrumb, Button, Container, Divider, Header, Icon, Popup, Segment, Table} from 'semantic-ui-react'
import {toast} from 'react-toastify'
import EstudianteService from '../../app/api/EstudianteService'
import {openModal,closeModal} from '../../app/store/actions/modalActions'
import EstudianteForm from '../../component/estudiantes/EstudianteForm';

const actions={
    openModal,
    closeModal
}
const Estudiantes = ({openModal,closeModal}) => {
    const [estudiantes,setEstudiantes]=useState([])
    const fetchEstudiantes=useCallback(async ()=>{
        try {
           const estudiantes=await EstudianteService.fetchEstudiantes()
           if(estudiantes) setEstudiantes(estudiantes)
        } catch (error) {
            toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
    },[])
    useEffect(()=>{
        fetchEstudiantes()
    },[fetchEstudiantes])

    const handleCreateOrEdit=async (values)=>{
        const estudiantesUpdateList=[...estudiantes]
        try {
            if(values.id){
                const updateEstudiante=await EstudianteService.updateEstudiante(values)
                const index=estudiantesUpdateList.findIndex((a)=>a.id===values.id)
                estudiantesUpdateList[index]=updateEstudiante
                toast.success('El Estudiante fue Actualizado',
                    {
                        position:"top-center",
                        autoClose:2000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
            }else{
                const estudiante={
                    nombres:values.nombres,
                    apellidos:values.apellidos,
                    genero:values.genero,
                    dni:values.dni,
                    fechaNacimiento:values.fechaNacimiento
                }
                const newEstudiante=await EstudianteService.addEstudiante(estudiante)
                estudiantesUpdateList.push(newEstudiante)
                toast.info('El Estudiante fue registrado',
                    {
                        position:"top-center",
                        autoClose:2000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
            }
            closeModal()
            setEstudiantes(estudiantesUpdateList)
        } catch (error) {
            toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
    }
    const handleDeleteEstudiante=async (id)=>{
        try {
            let estudianteUpdatedList=[...estudiantes]
            await EstudianteService.removeEstudiante(id)
            estudianteUpdatedList=estudianteUpdatedList.filter((a)=>a.id !== id)
            setEstudiantes(estudianteUpdatedList)
            toast.info('Estudiante Eliminado',
                    {
                        position:"top-center",
                        autoClose:2000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        } catch (error) {
            toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
    }
    let estudiantesList=<h4>No hay estudiantes registrados</h4>
    if(estudiantes && estudiantes.length>0){
        estudiantesList=(
            <Table celled selectable>
                <Table.Header>
                    <Table.Row>
                    <Table.HeaderCell>Documento</Table.HeaderCell>
                    <Table.HeaderCell>Nombres</Table.HeaderCell>
                    <Table.HeaderCell>Apellidos</Table.HeaderCell>
                    <Table.HeaderCell>Genero</Table.HeaderCell>
                    <Table.HeaderCell>Edad</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {estudiantes.map((estudiante)=>(
                        <Table.Row key={estudiante.id}>
                        <Table.Cell>{estudiante.dni}</Table.Cell>
                        <Table.Cell>{estudiante.nombres}</Table.Cell>
                        <Table.Cell>{estudiante.apellidos}</Table.Cell>
                        <Table.Cell>{estudiante.genero}</Table.Cell>
                        <Table.Cell>{estudiante.edad}</Table.Cell>
                        <Table.Cell>
                            <Popup
                                inverted
                                content="Actualizar Estudiante"
                                trigger={
                                    <Button
                                        color="blue"
                                        icon="edit"
                                        onClick={()=>openModal(<EstudianteForm estudianteId={estudiante.id} handleSubmit={handleCreateOrEdit}/>)}
                                    />
                                }
                            />
                            <Popup
                                inverted
                                content="Eliminar Estudiante"
                                trigger={
                                    <Button
                                        color="red"
                                        icon="trash"
                                        onClick={()=>{handleDeleteEstudiante(estudiante.id)}}
                                    />
                                }
                            />
                        </Table.Cell>
                    </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        )
    }
    return (
       <>
        <Segment>
           <Breadcrumb size='big'>
           <Breadcrumb.Section>Mantenimientos</Breadcrumb.Section>
           <Breadcrumb.Divider icon="right chevron" />
           <Breadcrumb.Section active>Estudiantes</Breadcrumb.Section>
           </Breadcrumb>
           <Divider horizontal>
                <Header as="h4">
                    <Icon name="list alternate outline" />
                    Lista de Estudiantes
                </Header>
           </Divider>
           <Segment textAlign='center'>
               <Button size='large' content="Nuevo Estudiante" icon='add user' color='grey' onClick={()=>openModal(<EstudianteForm handleSubmit={handleCreateOrEdit}/>)}/>
           </Segment>
           <Container textAlign='center'>
                {estudiantesList}
           </Container>
        </Segment>
       </>
    )
}

export default connect(null,actions) (Estudiantes)
