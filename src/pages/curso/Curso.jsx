import React,{useCallback, useState} from 'react'
import {Breadcrumb,Segment,Divider,Header,Button,Icon,Container,Table,Popup} from 'semantic-ui-react';
import {openModal,closeModal} from '../../app/store/actions/modalActions'
import {toast} from 'react-toastify'
import  CursoService from '../../app/api/CursoService'
import { useEffect } from 'react';
import { connect } from 'react-redux';
import CursoForm from '../../component/cursos/CursoForm';

const actions={
    openModal,
    closeModal
}
const Curso = ({openModal,closeModal}) => {
    const [cursos,setCursos]=useState([])
    const fetchCursos=useCallback(async()=>{
        try {
            const cursos=await CursoService.fetchCursos()
            if(cursos)setCursos(cursos)
        } catch (error) {
            toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
    },[])
    useEffect(()=>{
        fetchCursos()
    },[fetchCursos])
    const changeStateCurso=async (id)=>{
        try {
            const cursosUpdateList=[...cursos]
            const updateCurso=await CursoService.changeStateCurso(id,false)
            const index=cursosUpdateList.findIndex((a)=>a.id===id)
            cursosUpdateList[index]=updateCurso
            setCursos(cursosUpdateList)
            toast.warning('Curso Eliminado',
                    {
                        position:"top-center",
                        autoClose:2000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        } catch (error) {
            toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
    }
    const handleCreateOrEdit=async (values)=>{
        const cursosUpdateList=[...cursos]
        try {
            if(values.id){
                const updateCurso=await CursoService.updateCurso(values)
                const index=cursosUpdateList.findIndex((a)=>a.id===values.id)
                cursosUpdateList[index]=updateCurso
                toast.success('El Curso fue Actualizado',
                    {
                        position:"top-center",
                        autoClose:2000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
            }else{
                const curso={
                    nombreCurso:values.nombreCurso,
                    siglas:values.siglas,
                    precio:values.precio,
                    estado:values.estado
                }
                const newCurso=await CursoService.addCurso(curso)
                cursosUpdateList.push(newCurso)
                toast.info('El Curso fue registrado',
                    {
                        position:"top-center",
                        autoClose:2000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
            }
            setCursos(cursosUpdateList)
        } catch (error) {
            toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
        closeModal()
    }
    let cursosList=<h4>No hay cursos registrados</h4>
    if(cursos && cursos.length>0){
        cursosList=(
            <Table celled selectable>
                <Table.Header>
                    <Table.Row>
                    <Table.HeaderCell>Curso</Table.HeaderCell>
                    <Table.HeaderCell>Siglas</Table.HeaderCell>
                    <Table.HeaderCell>Precio</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {cursos.map((curso)=>(
                        <Table.Row key={curso.id} className={curso.estado?'positive':'negative'}>
                        <Table.Cell>{curso.nombreCurso}</Table.Cell>
                        <Table.Cell>
                            {!curso.estado?(<Icon name='warning'/>):(<></>)}
                            {curso.siglas}
                        </Table.Cell>
                        <Table.Cell>{curso.precio}</Table.Cell>
                        <Table.Cell>
                            <Popup
                                inverted
                                content="Actualizar Curso"
                                trigger={
                                    <Button
                                        color="blue"
                                        icon="edit"
                                        onClick={()=>openModal(<CursoForm cursoId={curso.id} handleSubmit={handleCreateOrEdit}/>)}
                                    />
                                }
                            />
                            {curso.estado?(
                                <Popup
                                inverted
                                content="Eliminar Curso"
                                trigger={
                                    <Button
                                        color="red"
                                        icon="trash"
                                        onClick={()=>changeStateCurso(curso.id)}
                                    />
                                }
                            />
                            ):(<></>)}
                        </Table.Cell>
                    </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        )
    }
    return (
        <Segment>
           <Breadcrumb size='big'>
           <Breadcrumb.Section>Mantenimiento</Breadcrumb.Section>
           <Breadcrumb.Divider icon="right chevron" />
           <Breadcrumb.Section active>Cursos</Breadcrumb.Section>
           </Breadcrumb>
           <Divider horizontal>
                <Header as="h4">
                    <Icon name="list alternate outline" />
                    Lista de Cursos
                </Header>
           </Divider>
           <Segment textAlign='center'>
               <Button size='large' content="Nuevo Curso" icon='book' color='grey' onClick={()=>openModal(<CursoForm handleSubmit={handleCreateOrEdit}/>)}/>
           </Segment>
           <Container textAlign='center'>
                {cursosList}
           </Container>
        </Segment>
    )
}

export default connect(null,actions) (Curso)
