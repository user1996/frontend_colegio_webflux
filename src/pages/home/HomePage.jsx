import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { Button ,Container, Header, Segment,Icon} from 'semantic-ui-react'
import PropTypes from 'prop-types'
import {getJwt} from '../../app/config/auth/credentials'
import {openModal} from '../../app/store/actions/modalActions'
import LoginForm from '../../component/auth/LoginForm'

const mapState = (state) => ({
  currentUser: state.auth.currentUser,
  token: getJwt(),
})
const actions = {
  openModal,
}
const HomePage = ({currentUser,token,openModal}) => {
    return (
        <Segment inverted textAlign='center' vertical className="masthead">
        <Container text>
          <Header as='h1' inverted>
            <Icon name='book' />Sistema de Matriculas
          </Header>
          {currentUser && token ?(
            <>
            <Header as='h2' inverted content='Welcome to the  best place to find the best food' />
            <Button size='huge' inverted content="Go to the Matriculas" as={Link} to='/matriculas' />
            </>
          ):(
            <>
            <Button size='huge' inverted content="Go to the Login" onClick={()=>openModal(<LoginForm/>)} />
            </> 
          )}
        </Container>
      </Segment>
    )
}
HomePage.propTypes = {
  openModal: PropTypes.func.isRequired,
  currentUser: PropTypes.object,
  token: PropTypes.string,
}

HomePage.defaultProps = {
  currentUser: null,
  token: null,
}
export default connect(mapState,actions) (HomePage)
