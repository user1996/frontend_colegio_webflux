import React, { useCallback, useEffect, useState } from 'react'
import { Breadcrumb, Button, Container, Divider, Header, Icon, List, Popup, Segment, Table } from 'semantic-ui-react'
import {toast} from 'react-toastify'
import Moment from 'react-moment'
import PropTypes from 'prop-types'

import LoadingComponent from '../../component/common/LoadingComponent'
import MatriculaService from '../../app/api/MatriculaService'

const Matriculas = ({history}) => {
    const [matriculas,setMatriculas]=useState([])
    const [loading,setLoading]=useState(false)
    const fetchMatriculas=useCallback(async ()=>{
        setLoading(true)
        try {
           const matriculas=await MatriculaService.fetchMatriculas()
           if(matriculas) setMatriculas(matriculas)
        } catch (error) {
            toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
        }
        setLoading(false)
    },[])
    useEffect(()=>{
        fetchMatriculas()
    },[fetchMatriculas])

        const handleDeleteMatricula=async(id)=>{
            setLoading(true)
            try {
                let matriculaUpdateList=[...matriculas]
                await MatriculaService.removeMatricula(id)
                matriculaUpdateList=matriculaUpdateList.filter((a)=>a.id !==id)
                setMatriculas(matriculaUpdateList)
                setLoading(false)
                toast.info('Matricula  Eliminada',
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
                    setLoading(false)
            } catch (error) {
                setLoading(false)
                toast.error('Error : '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
            }
        }

    let matriculasList=<h4>No hay Matriculas registradas</h4>
    if(matriculas && matriculas.length>0){
        matriculasList=(
            <Table celled selectable>
                <Table.Header>
                    <Table.Row>
                    <Table.HeaderCell>Documento</Table.HeaderCell>
                    <Table.HeaderCell>Estudiante</Table.HeaderCell>
                    <Table.HeaderCell>Fecha</Table.HeaderCell>
                    <Table.HeaderCell>Cursos</Table.HeaderCell>
                    <Table.HeaderCell>Registrado por</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {matriculas.map((matricula)=>(
                        <Table.Row key={matricula.id}>
                        <Table.Cell>{matricula.documento}</Table.Cell>
                        <Table.Cell>{matricula.estudiante}</Table.Cell>
                        <Table.Cell><Moment format="YYYY-MM-DD">{matricula.fecha}</Moment></Table.Cell>
                        <Table.Cell>
                            {matricula.cursos.map((c)=>(
                                <List key={c} bulleted>
                                    <List.Item>{c}</List.Item>
                                </List>
                            ))}
                        </Table.Cell>
                        <Table.Cell>{matricula.registradoPor}</Table.Cell>
                        <Table.Cell>
                            <Popup
                                inverted
                                content="Detalles"
                                trigger={
                                    <Button
                                        color="blue"
                                        icon="info"
                                        onClick={()=>history.push(`/matricula/${matricula.id}`)}
                                    />
                                }
                            />
                            <Popup
                                inverted
                                content="Eliminar Matriculado"
                                trigger={
                                    <Button
                                        color="red"
                                        icon="trash"
                                        onClick={()=>handleDeleteMatricula(matricula.id)}
                                    />
                                }
                            />
                        </Table.Cell>
                    </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        )
    }
    if (loading) return <LoadingComponent content="Cargando matriculas..." />
    return (
        <>
        <Segment>
           <Breadcrumb size='big'>
           <Breadcrumb.Section>Matriculas</Breadcrumb.Section>
           <Breadcrumb.Divider icon="right chevron" />
           <Breadcrumb.Section active>Listado</Breadcrumb.Section>
           </Breadcrumb>
           <Divider horizontal>
                <Header as="h4">
                    <Icon name="list alternate outline" />
                    Lista de Matriculados
                </Header>
           </Divider>
           <Container textAlign='center'>
                {matriculasList}
            </Container>
        </Segment>
        </>
    )
}
Matriculas.propTypes={
    history:PropTypes.object.isRequired
}
export default Matriculas