import React,{ useState ,useCallback,useEffect} from 'react'
import {toast} from 'react-toastify'
import Moment from 'react-moment'
import { Link } from 'react-router-dom'
import { Breadcrumb, Segment,Container,Grid,Divider,Icon,Header, Table, Label} from 'semantic-ui-react'

import LoadingComponent from '../../component/common/LoadingComponent'
import MatriculaService from '../../app/api/MatriculaService'

const MatriculaDetalles = ({match}) => {
    const [matricula,setMatricula]=useState(null)
    const [loading,setLoading]=useState(false)
    const fetchMatricula=useCallback(async()=>{
        setLoading(true)
        try {
            const matricula=await MatriculaService.fetchDetalleMatricula(match.params.id)
            if(matricula){
                const cursos=matricula.cursos;
                const totalCurso=cursos.reduce((sum,{precio})=>sum+precio,0)
                matricula.total=totalCurso
                setMatricula(matricula)
            }
            setLoading(false)
        } catch (error) {
            toast.error('Error '+error,
                    {
                        position:"top-center",
                        autoClose:5000,
                        hideProgressBar:false,
                        closeOnClick:true,
                        pauseOnHover:true,
                        draggable:true,
                        progress:undefined
                    })
                    setLoading(false)
        }
    },[match.params.id])
    useEffect(()=>{
        fetchMatricula()
    },[fetchMatricula])
    if(loading) return<LoadingComponent content="Cargando Detalle de Factura"/>
    let facturaDetalleArea=<h4>Detalle de factura</h4>
    if(matricula){
        facturaDetalleArea=(
            <Segment.Group>
                <Segment>
                    <Header as='h4' block color="violet">Estudiante</Header>
                </Segment>
                <Segment.Group>
                    <Segment>
                        <p><strong>Documento : </strong>{`${matricula.estudiante.dni}`}</p>
                        <p><strong>Nombre : </strong>{`${matricula.estudiante.nombres} ${matricula.estudiante.apellidos}`}</p>
                    </Segment>
                </Segment.Group>
                <Segment>
                    <Header as="h4" block color="violet">Matricula</Header>
                </Segment>
                <Segment.Group>
                    <Segment>
                        <p><strong>Codigo de matricula : </strong>{matricula.id}</p>
                        <p><strong>Fecha de  matricula : </strong><Moment format="YYYY/MM/DD">{matricula.fechaRegistro}</Moment></p>
                    </Segment>
                </Segment.Group>
                <Segment>
                    <Table celled striped color="violet">
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan='4'>
                                    <Icon name='list layout'/>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Curso</Table.HeaderCell>
                                <Table.HeaderCell>Siglas</Table.HeaderCell>
                                <Table.HeaderCell>Precio</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {matricula.cursos.length>0 && 
                                matricula.cursos.map((item)=>(
                                    <Table.Row key={item.id}>
                                        <Table.Cell>{item.nombreCurso}</Table.Cell>
                                        <Table.Cell>{item.siglas}</Table.Cell>
                                        <Table.Cell>{item.precio}</Table.Cell>
                                    </Table.Row>
                                ))
                            }
                        </Table.Body>
                    </Table>
                    <Container textAlign="right">
                            <Label basic color="violet" size="large">
                                Total :
                                <Label.Detail content={`S/${+matricula.total.toFixed(2)}`}/>
                            </Label>
                    </Container>
                </Segment>
            </Segment.Group>
        )
    }

    return (
        <Segment>
            <Breadcrumb size="large">
                <Breadcrumb.Section>Matricula</Breadcrumb.Section>
                <Breadcrumb.Divider icon="right chevron"/>
                <Breadcrumb.Section as={Link} to="/matriculas">
                    Lista de Matriculas
                </Breadcrumb.Section>
                <Breadcrumb.Divider icon="right chevron"/>
                <Breadcrumb.Section active>Detalle de Matricula</Breadcrumb.Section>
            </Breadcrumb>
            <Divider horizontal>
                <Header as="h4">
                <Icon name="address card outline" />
                Detalle  de Matricula
                </Header>
            </Divider>
            <Container>
                <Grid columns="3">
                <Grid.Column width="16">{facturaDetalleArea}</Grid.Column>
                </Grid>
            </Container>
        </Segment>
    )
}

export default MatriculaDetalles
