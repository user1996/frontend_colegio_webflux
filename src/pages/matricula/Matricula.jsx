import React from 'react'
import { Breadcrumb, Container, Divider, Header, Icon, Segment} from 'semantic-ui-react'
import MatriculaForm from '../../component/matriculas/MatriculaForm'
const Matricula = () => {
 
    return (
        <>
        <Segment>
           <Breadcrumb size='big'>
           <Breadcrumb.Section>Matriculas</Breadcrumb.Section>
           <Breadcrumb.Divider icon="right chevron" />
           <Breadcrumb.Section active>Registro</Breadcrumb.Section>
           </Breadcrumb>
           <Divider horizontal>
                <Header as="h4">
                    <Icon name="write" />
                    Registrar nueva matricula
                </Header>
           </Divider>
           <Container textAlign='center'>
               <MatriculaForm/>
            </Container>
        </Segment>
        </>
    )
}

export default Matricula
